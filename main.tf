# base image
resource "libvirt_volume" "ubuntu" {
  name   = "ubuntu"
  pool   = "default"
  source = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_volume" "root" {
  name           = "root"
  base_volume_id = libvirt_volume.ubuntu.id
  pool           = "default"
  size           = var.disk
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "commoninit.iso"
  user_data      = templatefile("${path.module}/cloudinit.cfg", {
  	hostname = var.hostname,
	domain = var.domain
  })
  network_config = templatefile("${path.module}/netconfig.cfg", {})
  pool           = "default"
}

resource "libvirt_domain" domain {
  name       = var.hostname
  memory     = var.memory
  vcpu       = var.vcpu
  qemu_agent = true

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    bridge         = "br0"
    hostname       = var.hostname
    wait_for_lease = true
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.root.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

output "domain" {
  value = libvirt_domain.domain.network_interface.0
}
