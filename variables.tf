variable "hostname" {
  description = "System hostname"
  default     = "scratch"
}

variable "domain" {
  description = "System domain"
  default     = "42can.org"
}

variable "disk" {
  description = "Size of disk"
  default = "10737418240"
}

variable "memory" {
  description = "Amount of memory to allocate"
  default = "4096"
}

variable "vcpu" {
  description = "Number of vcpu cores to assign"
  default = 2
}

variable "keyfile" {
  description = "Path to SSH key file used for authentication"
  default     = "id_ed25519"
}

variable "libvirt-user" {
  description = "Username used to connect to libvirt host"
  default = ""
}

variable "libvirt-host" {
  description = "Hostname of target libvirt host"
  default = "localhost"
}

