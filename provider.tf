terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.7.6"
    }
  }
}

provider "libvirt" {
  uri = "qemu+ssh://${var.libvirt-user}@${var.libvirt-host}/system?sshauth=privkey&keyfile=${var.keyfile}&no_verify=1"
}
